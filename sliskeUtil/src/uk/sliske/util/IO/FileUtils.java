package uk.sliske.util.IO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

public class FileUtils {

	public static final String	SEPERATOR		= File.separator;
	public static final String	USER_HOME		= System.getProperty("user.home");
	public static final String	SLISKE_HOME		= formatPath(USER_HOME, "sliske");
	private static final String	SETTINGS_HOME	= formatPath(SLISKE_HOME, "settings");

	public static final String formatPath(String... tokens) {
		String result = tokens[0];
		for (int i = 1; i < tokens.length; i++) {
			result += SEPERATOR;
			result += tokens[i];
		}
		return result;
	}

	public static final boolean saveSettings(final String filename, final HashMap<String, String> settings) {
		try {
			Properties props = new Properties();
			for (Entry<String, String> e : settings.entrySet()) {
				props.setProperty(e.getKey(), e.getValue());
			}
			File d = new File(SETTINGS_HOME);
			if (!d.exists())
				d.mkdirs();
			File f = new File(d, filename);
			if (!f.exists()) {
				f.createNewFile();
			}
			OutputStream out = new FileOutputStream(f);

			props.store(out, "User properties");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static final HashMap<String, String> loadSettings(String filename) {
		HashMap<String, String> settings = new HashMap<String, String>();

		try {
			Properties props = new Properties();
			File d = new File(SETTINGS_HOME);
			if (!d.exists())
				return settings;
			File f = new File(d, filename);
			if (!f.exists()) {
				return settings;
			}
			InputStream in = new FileInputStream(f);
			props.load(in);

			for (Entry<Object, Object> e : props.entrySet()) {
				settings.put(e.getKey().toString(), e.getValue().toString());
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		return settings;
	}

	public static void main(String[] args) {
		HashMap<String, String> settings = new HashMap<String, String>();
		settings.put("rsbot", formatPath(USER_HOME, "Desktop", "RSBot.jar"));

		saveSettings("rsbot_loader", settings);
		
		HashMap<String, String> loaded = loadSettings("rsbot_loader");
		for (Entry<String, String> e : loaded.entrySet()) {
			System.out.printf("%s : %s\n", e.getKey(), e.getValue());
		}
	}

}
