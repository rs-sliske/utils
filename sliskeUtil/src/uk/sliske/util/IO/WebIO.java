package uk.sliske.util.IO;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;

import uk.sliske.util.SliskeError;
import uk.sliske.util.graphics.Image;

public class WebIO {
	private static final String	DIRECTORY	= FileUtils
													.formatPath(FileUtils.SLISKE_HOME, "cache", "images");	;

	public static BufferedImage loadBImageFromWeb(URL url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection
				.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36");
		final BufferedImage image = ImageIO.read(connection.getInputStream());
		return image;
	}

	public static BufferedImage loadBImageFromWeb(String address) throws IOException {
		File d = new File(DIRECTORY);
		String filename = getFileName(address);
		File f = new File(d, filename);
		if (f.exists()) {
			BufferedImage image = ImageIO.read(f);
			if (image != null)
				return image;
		}

		BufferedImage image = loadBImageFromWeb(new URL(address));

		if (address.contains("sliske.uk/img")) {
			if (!d.exists()) {
				d.mkdirs();
			}
			if (d.exists()) {
				try {
					ImageIO.write(image, "png", f);
				} catch (IOException e) {
				}
			}
		}
		return image;

	}

	public static Image loadImageFromWeb(String address) throws IOException {
		BufferedImage temp = loadBImageFromWeb(address);
		if (temp == null) {
			new SliskeError("null image", new Exception().getStackTrace()).show();
		}
		return new Image(temp);
	}

	private static String getFileName(String url) {
		String[] tokens = url.split("/");
		return tokens[tokens.length - 1];
	}
}
