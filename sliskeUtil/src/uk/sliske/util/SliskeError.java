package uk.sliske.util;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import uk.sliske.util.graphics.Window;

public class SliskeError {

	private static ArrayList<SliskeError>	errors		= new ArrayList<SliskeError>();

	private ArrayList<String>				stackTraceLines;
	private ArrayList<String>				messageLines;

	private SliskeError() {
		errors.add(this);
		stackTraceLines = new ArrayList<String>();
		messageLines = new ArrayList<String>();
	}

	public SliskeError(String message) {
		this();

		messageLines.add(message);
	}

	public SliskeError(StackTraceElement[] stackTrace) {
		this();
		for (StackTraceElement line : stackTrace) {
			stackTraceLines.add(line.toString());
		}
		show();
	}

	public SliskeError(String message, StackTraceElement[] stackTrace) {
		this(message);

		for (StackTraceElement line : stackTrace) {
			stackTraceLines.add(line.toString());
		}
		show();
	}

	public void show() {
		System.out.println("error");
		addToWindow(createFrame());
	}

	private static void createWindow() {
		JFrame frame = new Window("Error", "Error", 500, 500).getFrame();
		frame.setBounds(100, 100, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 434, 261);
		frame.getContentPane().add(tabbedPane);
		frame.setContentPane(tabbedPane);

		
	}
	private static void addToWindow(JPanel panel){
		if(Window.get("Error")==null){
			createWindow();
		}
		Window.get("Error").getFrame().getContentPane().add(panel);
	}
	
	private JPanel createFrame(){
		JPanel frame = new JPanel();
		JTextArea msgArea = new JTextArea();
		msgArea.setBounds(10, 11, 434, 92);
		frame.add(msgArea);

		JTextArea stackArea = new JTextArea();

		JScrollPane scrollPane = new JScrollPane(stackArea);
		scrollPane.setBounds(10, 114, 464, 336);
		frame.add(scrollPane);

		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(417, 0, 17, 336);
		scrollPane.add(scrollBar);

		for (String s : stackTraceLines) {
			stackArea.append(s);
			stackArea.append("\n");
		}
		for (String s : messageLines) {
			msgArea.append(s);
		}
		return frame;
	}

}
