package uk.sliske.util;

import java.util.ArrayList;

public class Strings {

	public static boolean stringEndsWith(String str, ArrayList<String> args) {
		return stringEndsWith(str, (String[]) args.toArray());
	}

	public static boolean stringEndsWith(String str, String[] args) {
		for(String s:args){
			if(str.endsWith(s)){
				return true;
			}
		}
		return false;
	}

}
