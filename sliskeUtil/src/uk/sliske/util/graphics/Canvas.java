package uk.sliske.util.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class Canvas extends java.awt.Canvas {
	private static final long	serialVersionUID	= 1L;

	private final Image			image;
	private final String		alt;

	public Canvas(Image image, String altText) {
		this.image = image;
		this.alt = altText;
	}
	public Canvas(Image image) {
		this(image, "");
	}

	@Override
	public void paint(Graphics g) {	
		g.setColor(Color.white);
		if (image != null)
			g.drawImage(image, 0, 0, null);
		else {
			g.drawString(alt, 50, 50);
			g.drawRoundRect(0, 0, getWidth()-1, getHeight()-1, 5, 5);
			
		}
			
	}
}
